package no.hig.drawing;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Simple demo of custom view. 
 * @author mariusz
 */
public class MainActivity extends Activity {

	private CustomView cv;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.cv = new CustomView(this);
		final RelativeLayout mainView = (RelativeLayout) findViewById(R.id.mainView);
		mainView.addView(cv);
		
		final Button btnMoveRight = (Button) findViewById(R.id.btn_move_right);
		btnMoveRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				cv.postDelayed(moveToRight, 20);
			}
		});
	}
	
	private Runnable moveToRight = new Runnable() {
		public void run() {
			float x = cv.getCircleX();
			float y = cv.getCircleY();
			float r = cv.getCircleRadius();
			cv.setCircleLocation(x + 50, y, r);		
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
