/**
 * 
 */
package no.hig.drawing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Simple example of custom view.
 * @author mariusz
 *
 */
public class CustomView extends View {

	// position and radius of the circle
	float x = 50; 
	float y = 50;
	float radius = 20;
	Paint paint;
	
	public CustomView(Context context) {
		super(context);
		this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		this.paint.setColor(Color.GREEN);

	}
	
	public void setCircleLocation(float nx, float ny, float nr) {
		this.x = nx;
		this.y = ny;
		this.radius = nr;
		invalidate();
	}
	
	@Override
	public void onDraw(final Canvas canvas) {
		canvas.drawCircle(x, y, radius, paint);
	}
	
	public float getCircleX() {
		return this.x;
	}
	
	public float getCircleY() {
		return this.y;
	}

	public float getCircleRadius() {
		return this.radius;
	}

}
